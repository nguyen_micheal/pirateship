package sg.games.pirate.entities;

import com.jme3.app.Application;
import com.jme3.scene.Spatial;
import sg.atom.ai.agents.Agent;
import sg.games.pirate.gameplay.Cost;

/**
 * Unit is the basic Entity which has Spatial and Agent.
 *
 * Sketch of AI.
 *
 * -find the target
 *
 * - go straight to the target if no particular danger
 *
 * - go in zigzag to the target if there is danger
 *
 * - shoot if close enough (in range)
 *
 * - die or explode if out of health
 *
 * @author cuong.nguyen
 */
public class Ship extends BaseShooterCharacter {

    //Attributes
    protected int hitPoint;
    protected int maxHitPoint;
    protected int attack;
    protected int defense;
    protected int mana;
    protected int maxMana;
    protected Cost cost;
    protected Cost upgradeCost;
    
    public Ship() {
    }

    public Ship(long iid, String type, Spatial spatial) {
        super(iid, type, spatial);
    }

    @Override
    public void resetAttributes() {
        maxHitPoint = 1000;
        hitPoint = maxHitPoint;
        maxMana = 1000;
        mana = maxMana;
    }

    @Override
    public void init(Application app) {
        super.init(app);
        resetAttributes();
        addFunctions();
    }

    protected void addFunctions() {
    }

    public int getAttack() {
        return attack;
    }

    public int getDefense() {
        return defense;
    }

    public int getMana() {
        return mana;
    }

    public void letAttack() {
    }

    public void letDefense() {
    }

    public void letDoSkill() {
    }

    public void letDie() {
        deactivate();
        removeFromStage();
    }

    public Cost getCost() {
        return cost;
    }

    public void setCost(Cost cost) {
        this.cost = cost;
    }

    public Cost getUpgradeCost() {
        return upgradeCost;
    }

    public void setUpgradeCost(Cost upgradeCost) {
        this.upgradeCost = upgradeCost;
    }

    public void letDamaged(int hitPoint) {
        decreaseHitPoints(hitPoint);
    }

    /**
     * Method for increasing agents hitPoint for fixed amount. If adding
     * hitPoint will cross the maximum hitPoint of agent, then agent's hitPoint
     * status will be set to maximum allowed hitPoint for that agent.
     *
     * @see Agent#maxHitPoint
     * @param addHitPoint amount of hitPoint that should be added
     */
    public void increaseHitPoint(int addHitPoint) {
        hitPoint += addHitPoint;
        if (hitPoint > maxHitPoint) {
            hitPoint = maxHitPoint;
        }
    }

    public float getHitPoint() {
        return hitPoint;
    }

    public void setHitPoint(int hitPoint) {
        if (maxHitPoint < hitPoint) {
            this.hitPoint = maxHitPoint;
        }
        this.hitPoint = hitPoint;
    }

    public float getMaxHitPoint() {
        return maxHitPoint;
    }

    public void setMaxHitPoint(int maxHitPoint) {
        this.maxHitPoint = maxHitPoint;
    }

    /**
     * Method for decreasing agents hitPoint for fixed amount. If hitPoint drops
     * to zero or bellow, agent's hitPoint status will be set to zero and he
     * will be dead.
     *
     * @see Agent#alive
     * @param damage amount of hitPoint that should be removed
     */
    public void decreaseHitPoints(int damage) {
        hitPoint -= damage;
        if (hitPoint <= 0) {
            hitPoint = 0;
            letDie();
            System.out.println("Let die");
        }
    }

}
