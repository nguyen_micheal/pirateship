package sg.games.pirate.entities;

import com.jme3.app.Application;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;

/**
 * A cute witch which can dress up
 *
 * @author cuong.nguyen
 */
public class PlayerShip extends Ship {

    public PlayerShip(long iid, String type, Spatial spatial) {
        super(iid, type, spatial);
    }

    @Override
    public void init(Application app) {
        super.init(app);
        
        setMoveSpeed(1);
        setMoveVec(new Vector3f(0, 0, 0.05f));
    }
}
