package sg.games.pirate.entities;

import sg.atom.ai.agents.Agent;
import com.jme3.math.Vector3f;
import sg.games.pirate.entities.controls.ProjectileControl;
import sg.games.pirate.entities.controls.WeaponControl;

/**
 *
 * @author Tihomir Radosavljevic
 * @version 1.0
 */
public class Weapon extends WeaponControl {

    public Weapon(Agent agent) {
        this.agent = agent;
        init();
    }

    public void init() {
        cooldown = 0.4f;
        attackDamage = 20f;
        numberOfBullets = -1;
        maxAttackRange = 1000f;
        minAttackRange = 3f;
    }

    @Override
    protected ProjectileControl createProjectile(Vector3f direction, float tpf) {
        return null;
    }
}
