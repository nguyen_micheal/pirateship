package sg.games.pirate.entities;

import sg.atom.corex.stage.BaseCharacter;
import com.jme3.scene.Spatial;
import sg.games.pirate.PirateMain;
import sg.games.pirate.ai.GameAgent;
import sg.games.pirate.gameplay.ShipGamePlay;

/**
 * Unit is the basic Entity which has Spatial and Agent.
 *
 * Sketch of AI.
 *
 * -find the target
 *
 * - go straight to the target if no particular danger
 *
 * - go in zigzag to the target if there is danger
 *
 * - shoot if close enough (in range)
 *
 * - die or explode if out of health
 *
 * @author cuong.nguyen
 */
public class BaseShooterCharacter extends BaseCharacter {

    protected Weapon weapon;

    public BaseShooterCharacter() {
        super(0, null, null);
    }

    public BaseShooterCharacter(long iid, String type, Spatial spatial) {
        super(iid, type, spatial);
    }

    @Override
    protected void initAgent() {
        this.agent = new GameAgent(type + getIid(), spatial);
        this.agent.init(this.app);
        this.agent.setModel(this);
    }

    public void createWeapon() {
        this.weapon = new Weapon(agent);
        this.weapon.init(getApp());
        getAgent().setWeaponControl(weapon);
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon gun) {
        this.weapon = gun;
        getAgent().setWeaponControl(weapon);
    }

    protected ShipGamePlay getGamePlay() {
        return getApp().getGamePlayManager().getCurrentGamePlay();
    }

    @Override
    public PirateMain getApp() {
        return (PirateMain) super.getApp();
    }

    @Override
    public GameAgent<BaseCharacter> getAgent() {
        return (GameAgent<BaseCharacter>) super.getAgent();
    }

}
