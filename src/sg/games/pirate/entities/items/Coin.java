/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.games.pirate.entities.items;

import com.jme3.scene.Spatial;
import sg.atom.corex.entity.SpatialEntity;

/**
 *
 * @author cuongnguyen
 */
public class Coin extends SpatialEntity {

    public Coin(long iid, String type, Spatial spatial) {
        super(iid, type, spatial);
    }

    @Override
    public void activate() {
        super.activate();
    }

    @Override
    public void deactivate() {
        super.deactivate();
        removeFromStage();
    }

}
