package sg.games.pirate.entities;

import com.jme3.scene.Spatial;
import sg.atom.corex.entity.SpatialEntity;

/**
 *
 * @author cuongnguyen
 */
public class Bullet extends SpatialEntity {

    public Bullet(long iid, String type, Spatial spatial) {
        super(iid, type, spatial);
    }

    @Override
    public void activate() {
        super.activate(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deactivate() {
        super.deactivate();
        
        removeFromStage();
    }
    
    
}
