package sg.games.pirate.entities;

import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;

/**
 * Enemy witch
 *
 * @author cuong.nguyen
 */
public class EnemyShip extends Ship {

    public EnemyShip(long iid, String type, Spatial spatial) {
        super(iid, "AirCraft", spatial);
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        Vector3f localTranslation = this.spatial.getLocalTranslation().clone();
        this.spatial.setLocalTranslation(localTranslation.add(0, 0, getMoveSpeed() * tpf));

    }
}
