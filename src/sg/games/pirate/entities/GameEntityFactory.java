package sg.games.pirate.entities;

import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import sg.atom.ai.agents.control.AgentsAppState;
import sg.atom.core.AtomMain;
import sg.atom.corex.entity.ComposableEntity;
import sg.atom.corex.entity.EntityFactory;
import sg.atom.corex.entity.SpatialEntityControl;
import sg.games.pirate.PirateMain;
import sg.games.pirate.entities.controls.PlayerCharacterControl;
import sg.games.pirate.entities.controls.BulletControl;
import sg.games.pirate.entities.controls.WeaponControl;
import sg.games.pirate.entities.items.Coin;

/**
 *
 * @author cuong.nguyen
 */
public class GameEntityFactory extends EntityFactory {

    public GameEntityFactory(AtomMain app) {
        super(app);
    }

    public ComposableEntity create(String type, Object... params) {
        ComposableEntity newEntity = null;

        //Piority orders: 1- prototype, 2- by method, 3-reflection 4-simple place holder.
        if (type.equalsIgnoreCase("Tree")
                || type.equalsIgnoreCase("Flag")) {
            newEntity = null;
        } else if (type.equalsIgnoreCase("Ship")) {
//            newEntity = createSoldier((Integer) params[0]);
        } else if (type.equalsIgnoreCase("Bullet")) {
            newEntity = createBullet((WeaponControl) params[0]);
        } else {
            newEntity = createSpatialEntity(type, SpatialEntityControl.class);
        }

        return newEntity;
    }

    public Coin createCoinAt(Vector3f pos) {
        Spatial model = getApp().getWorldManager().getModel("Coin").clone();
        Coin coin = new Coin(entityManager.getNewEntityId(), "Coin", model);
        coin.init(app);
        entityManager.addEntity(coin);
        coin.getSpatial().addControl(new SpatialEntityControl(entityManager, coin));
        getApp().getWorldManager().putEntity(coin, pos);
        return coin;
    }

    public Bullet createBullet(WeaponControl weaponControl) {
        Spatial model = createBulletSpatial();
        Bullet bullet = new Bullet(entityManager.getNewEntityId(), "Bullet", model);
        bullet.init(app);
        entityManager.addEntity(bullet);
        BulletControl bulletControl = new BulletControl(weaponControl, bullet);
        bullet.getSpatial().addControl(bulletControl);
        bulletControl.init(app);
        return bullet;
    }

    protected Geometry createBulletSpatial() {
        /* A colored lit cube. Needs light source! */
        Box boxMesh = new Box(0.4f, 0.4f, 0.4f);
        Geometry boxGeo = new Geometry("Colored Box", boxMesh);
        Material boxMat = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
//        boxMat.setBoolean("UseMaterialColors", true);
//        boxMat.setColor("Ambient", ColorRGBA.Green);
//        boxMat.setColor("Diffuse", ColorRGBA.Green);
        boxGeo.setMaterial(boxMat);
        return boxGeo;
    }
//

    public PlayerShip createPlayerShip() {
        Spatial model = getApp().getWorldManager().getModel("Ship1").clone();
        PlayerShip playerShip = new PlayerShip(entityManager.getNewEntityId(), "Ship1", model);
        playerShip.init(app);
        PlayerCharacterControl playerControl = new PlayerCharacterControl(playerShip);
        playerShip.getSpatial().addControl(playerControl);
        playerControl.init(app);
        entityManager.addEntity(playerShip);
        return playerShip;
    }
//    public Soldier createSoldier(int type) {
//        //Can use another mapping here?
//        Spatial model = app.getWorldManager().getModel("Warrior" + type).clone();
//        String subType = "Warrior" + type;
//        Soldier newEntity = new Soldier(entityManager.getNewEntityId(), subType, model);
//        newEntity.init(app);
//        entityManager.addEntity(newEntity);
//        newEntity.getSpatial().addControl(new SpatialEntityControl(entityManager, newEntity));
//
//        GameAgent<BaseCharacter> unitAgent = newEntity.getAgent();
//        getAgentAppState().addAgent(unitAgent);
//        unitAgent.setVisibilityRange(100f);
//        unitAgent.setMass(1);
//        unitAgent.setMoveSpeed(2.0f);
//        unitAgent.setRotationSpeed(1.0f);
//        unitAgent.setMaxForce(3);
//        unitAgent.setMainBehaviour(new NPCSoldierBehaviour(unitAgent));
//
//        Gun gun = new Gun(unitAgent);
//        gun.init(app);
//        newEntity.setWeapon(gun);
//
//        newEntity.activate();
//        return newEntity;
//    }

    private AgentsAppState getAgentAppState() {
        return getApp().getGamePlayManager().getCurrentGamePlay().getAgentsAppState();
    }

    public PirateMain getApp() {
        return (PirateMain) app;
    }

}
