/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.games.pirate.entities.controls;

import com.jme3.animation.AnimControl;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import java.util.List;

/**
 *
 * @author cuongnguyen
 */
public class CharacterAnimationControl extends GameSpatialControl {

    //Animations
    protected List<AnimControl> animationList;
    protected String[] animationNames = {"run"};

    public List<AnimControl> getAnimationList() {
        return animationList;
    }

    public void setAnimationList(List<AnimControl> animationList) {
        this.animationList = animationList;
    }

    public String[] getAnimationNames() {
        return animationNames;
    }

    public void setAnimationNames(String[] animationNames) {
        this.animationNames = animationNames;
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

}
