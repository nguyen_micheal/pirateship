package sg.games.pirate.entities.controls;

import com.jme3.math.Vector3f;
import sg.atom.ai.agents.Agent;
import sg.atom.corex.entity.SpatialEntity;

/**
 * Base class for bullets in game.
 *
 */
public abstract class ProjectileControl extends GameSpatialControl {

    /**
     * Weapon from which bullet was fired.
     */
    protected WeaponControl weapon;
    protected final SpatialEntity spatialEntity;
    protected int hitPoint = 100;
    protected int maxHitPoint = 100;
    protected Vector3f targetPos;
    protected Vector3f direction;
    protected float duration = 3;
    /**
     * Position where bullet appears.
     */
    private Vector3f startPos;

    /**
     * Constructor for AbstractBullet.
     *
     * @param weapon weapon from which bullet was fired
     * @param spatialEntity spatial for bullet
     */
    public ProjectileControl(WeaponControl weapon, SpatialEntity spatialEntity) {
        this.weapon = weapon;
        this.spatialEntity = spatialEntity;
    }

    public void start() {
        startPos = new Vector3f(spatialEntity.getSpatial().getLocalTranslation());
    }

    public void end() {

    }

    public boolean isEndLife() {
        return activeTime > duration;
    }

    /**
     * Get weapon.
     *
     * @return weapon from which bullet was fired
     */
    public WeaponControl getWeapon() {
        return weapon;
    }

    /**
     * Setting weapon.
     *
     * @param weapon weapon from which bullet was fired
     */
    public void setWeapon(WeaponControl weapon) {
        this.weapon = weapon;
    }

    /**
     * Method for increasing agents hitPoint for fixed amount. If adding
     * hitPoint will cross the maximum hitPoint of agent, then agent's hitPoint
     * status will be set to maximum allowed hitPoint for that agent.
     *
     * @see Agent#maxHitPoint
     * @param potionHitPoint amount of hitPoint that should be added
     */
    public void increaseHitPoint(int potionHitPoint) {
        hitPoint += potionHitPoint;
        if (hitPoint > maxHitPoint) {
            hitPoint = maxHitPoint;
        }
    }

    public int getHitPoint() {
        return hitPoint;
    }

    public void setHitPoint(int hitPoint) {
        if (maxHitPoint < hitPoint) {
            this.hitPoint = maxHitPoint;
        }
        this.hitPoint = hitPoint;
    }

    public int getMaxHitPoint() {
        return maxHitPoint;
    }

    public void setMaxHitPoint(int maxHitPoint) {
        this.maxHitPoint = maxHitPoint;
    }

    public SpatialEntity getSpatialEntity() {
        return spatialEntity;
    }

    public void setTarget(Vector3f targetPos) {
        this.targetPos = targetPos;
    }

    public void setDirection(Vector3f direction) {
        this.direction = direction;
    }

    public Vector3f getDirection() {
        return direction;
    }

    public Vector3f getTarget() {
        return targetPos;
    }
}
