package sg.games.pirate.entities.controls;

import com.jme3.animation.AnimControl;
import java.util.List;

/**
 *
 * @author cuongnguyen
 */
public class UnitAnimationControl {

    protected List<AnimControl> animationList;
    protected String[] animationNames = {"base_stand", "run_01", "shoot", "strike_sword"};

    public List<AnimControl> getAnimationList() {
        return animationList;
    }

    public void setAnimationList(List<AnimControl> animationList) {
        this.animationList = animationList;
    }

    public String[] getAnimationNames() {
        return animationNames;
    }

    public void setAnimationNames(String[] animationNames) {
        this.animationNames = animationNames;
    }
}
