package sg.games.pirate.entities.controls;

import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import sg.atom.ai.agents.Agent;
import sg.atom.core.AtomMain;
import sg.atom.corex.entity.SpatialEntity;
import sg.games.pirate.entities.Ship;

/**
 * Control flying trajectory of a bullet.
 */
public class BulletControl extends ProjectileControl {

    private Vector3f moveVec;
    /**
     * Position where bullet disapears and explosion starts.
     */
    private Vector3f contactPoint;

//    private float bulletLength;
//    private float bulletPathLength;
//    private Geometry geoRay;
    public BulletControl(WeaponControl weapon, SpatialEntity spatialEntity) {
        super(weapon, spatialEntity);

//        bulletLength = 70f;
    }

    @Override
    public void init(AtomMain app) {
        super.init(app);

        Spatial agentSpatial = weapon.getAgent().getSpatial();
        spatial.setLocalRotation(agentSpatial.getLocalRotation().clone());
        spatial.setLocalTranslation(agentSpatial.getLocalTranslation());

    }

    @Override
    protected void controlUpdate(float tpf) {
        //Check end of life
        if (isEndLife()) {
            end();
        } else {
            SpatialEntity entity = checkHitted();
            if (entity == null) {
                spatial.move(moveVec.mult(tpf));
            } else {
                explode();
            }
        }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

    protected SpatialEntity checkHitted() {
        SpatialEntity hittedSpatialEntity = getApp().getEntityManager().checkCollision(spatialEntity, getWeapon().getSpatial(), getWeapon().getAgent().getSpatial());
        if (hittedSpatialEntity != null) {
            if (hittedSpatialEntity instanceof Ship) {
                Ship hittedUnit = (Ship) hittedSpatialEntity;
                Agent hittedAgent = hittedUnit.getAgent();
                if (getApp().getGamePlayManager().getCurrentGamePlay().getAgentsAppState().isEnemy(hittedAgent, getWeapon().getAgent())) {
                    hittedUnit.letDamaged(this.getHitPoint());
                    return hittedUnit;
                } else {
                    System.out.println("Not enemy!");
                }
            } else {
                System.out.println("Not an unit!" + hittedSpatialEntity.toString());
            }
        }
        return null;
    }

    @Override
    public void setDirection(Vector3f direction) {
        super.setDirection(direction);
        moveVec = direction.clone();
    }

    protected void explode() {
        System.out.println("Explode");
        this.getSpatial().removeControl(this);
        this.spatialEntity.deactivate();
    }

}
