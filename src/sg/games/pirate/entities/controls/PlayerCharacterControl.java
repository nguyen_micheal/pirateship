package sg.games.pirate.entities.controls;

import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import sg.atom.core.AtomMain;
import sg.games.pirate.entities.PlayerShip;

/**
 *
 * @author cuongnguyen
 */
public class PlayerCharacterControl extends GameSpatialControl {

    private final PlayerShip playerCharacter;

    public PlayerCharacterControl(PlayerShip playerCharacter) {
        this.playerCharacter = playerCharacter;
    }

    @Override
    public void init(AtomMain app) {
        super.init(app);

        setupInput();
    }

    protected void setupInput() {
        InputManager inputManager = app.getInputManager();
        inputManager.addMapping("FlyLeft", new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("FlyRight", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addListener(actionListener, new String[]{"FlyLeft", "FlyRight"});
    }

    @Override
    public void controlUpdate(float tpf) {
        flyForward();
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

    /**
     * Use ActionListener to respond to pressed/released inputs (key presses,
     * mouse clicks)
     */
    private ActionListener actionListener = new ActionListener() {
        public void onAction(String name, boolean pressed, float tpf) {
            if (name.equals("FlyLeft") && pressed) {
                flyLeft();
            } else if (name.equals("FlyRight") && pressed) {
                flyRight();
            }
        }
    };

    protected void flyForward() {
        Vector3f currentPos = getSpatial().getLocalTranslation();
        getSpatial().setLocalTranslation(currentPos.add(playerCharacter.getMoveVec().mult(playerCharacter.getMoveSpeed())));
    }

    protected void flyLeft() {
        Vector3f currentPos = getSpatial().getLocalTranslation();
        getSpatial().setLocalTranslation(currentPos.add(new Vector3f(1, 0, 0).mult(playerCharacter.getMoveSpeed())));
    }

    protected void flyRight() {
        Vector3f currentPos = getSpatial().getLocalTranslation();
        getSpatial().setLocalTranslation(currentPos.add(new Vector3f(-1, 0, 0).mult(playerCharacter.getMoveSpeed())));
    }
}
