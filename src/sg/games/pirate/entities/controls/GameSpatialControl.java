package sg.games.pirate.entities.controls;

import sg.atom.corex.spatial.PhysicalSpatialControl;
import sg.games.pirate.PirateMain;

/**
 *
 * @author cuongnguyen
 */
public abstract class GameSpatialControl extends PhysicalSpatialControl {

    @Override
    public PirateMain getApp() {
        return (PirateMain) super.getApp();
    }

}
