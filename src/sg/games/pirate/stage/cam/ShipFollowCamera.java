package sg.games.pirate.stage.cam;

import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.event.TouchEvent;
import com.jme3.math.Vector3f;
import com.jme3.math.Vector4f;
import com.jme3.renderer.Camera;
import java.util.ArrayList;
import java.util.List;
import sg.atom.core.execution.BaseGameState;
import sg.games.pirate.entities.PlayerShip;

/**
 * ShooterCamera controls the Camera of the game.
 *
 * @author cuong.nguyen
 */
public class ShipFollowCamera extends BaseGameState {

    Camera cam;
    PlayerShip playerShip;
    boolean isTouchDevice = false;
    Vector4f camMoveBounds;
    boolean isEnableRotate = true;
    boolean isEnableTill = true;
    boolean isEnableDistance = true;
    boolean isEnableMove = true;
    List<CameraMoveListener> listeners;
    Vector3f offset;

    public static interface CameraMoveListener {

        public void onCamMove(int degree, float amount);
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);

        this.cam = app.getCamera();
        this.offset = new Vector3f(0, 120, -100);

        getApp().getFlyByCamera().setMoveSpeed(0);
        getApp().getInputManager().setCursorVisible(false);

        cam.setLocation(new Vector3f(0, 2, -5));
        cam.lookAtDirection(Vector3f.ZERO, Vector3f.UNIT_Y);
        listeners = new ArrayList<CameraMoveListener>();

//        System.out.println("Init ship cam!");
    }

    public void setPlayerShip(PlayerShip playerCharacter) {
        this.playerShip = playerCharacter;
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);

        if (playerShip != null) {
            Vector3f playerPos = playerShip.getSpatial().getLocalTranslation();
            cam.setLocation(playerPos.add(offset));
//            cam.lookAtDirection(playerPos, Vector3f.UNIT_Y);

//            System.out.println("Update ship cam!");
        }
    }

    public void onAction(String name, boolean isPressed, float tpf) {

        for (CameraMoveListener listener : listeners) {
        }
    }

    public void onTouch(String name, TouchEvent event, float tpf) {

        for (CameraMoveListener listener : listeners) {
        }
    }

    public Vector4f getCamMoveBounds() {
        return camMoveBounds;
    }

    public void setCamMoveBounds(Vector4f camMoveBounds) {
        this.camMoveBounds = camMoveBounds;
    }

    public boolean isIsEnableRotate() {
        return isEnableRotate;
    }

    public void setIsEnableRotate(boolean isEnableRotate) {
        this.isEnableRotate = isEnableRotate;
    }

    public boolean isIsEnableTill() {
        return isEnableTill;
    }

    public void setIsEnableTill(boolean isEnableTill) {
        this.isEnableTill = isEnableTill;
    }

    public boolean isIsEnableDistance() {
        return isEnableDistance;
    }

    public void setIsEnableDistance(boolean isEnableDistance) {
        this.isEnableDistance = isEnableDistance;
    }

    public boolean isIsEnableMove() {
        return isEnableMove;
    }

    public void setIsEnableMove(boolean isEnableMove) {
        this.isEnableMove = isEnableMove;
    }

    public void addListener(CameraMoveListener listener) {
        listeners.add(listener);
    }

    public List<CameraMoveListener> getListeners() {
        return listeners;
    }

    public void removeListener(CameraMoveListener listener) {
        listeners.remove(listener);
    }

    public void removeAllListeners() {
        listeners.clear();
    }
}
