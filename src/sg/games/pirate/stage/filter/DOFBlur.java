/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sg.games.pirate.stage.filter;

/**
 *
 * @author cuong.nguyenmanh2
 */
import com.jme3.asset.AssetManager;
import com.jme3.asset.TextureKey;
import com.jme3.material.Material;
import com.jme3.post.Filter;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.texture.Texture;

/**
 *
 */
public class DOFBlur extends Filter {

    Texture ccmap;

    @Override
    protected void initFilter(AssetManager manager, RenderManager renderManager, ViewPort vp, int w, int h) {
        material = new Material(manager, "MatDefs/DOFBlur.j3md");
    }

    @Override
    protected Material getMaterial() {
        return material;
    }
}