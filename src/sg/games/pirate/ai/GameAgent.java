package sg.games.pirate.ai;

import com.jme3.scene.Spatial;
import sg.atom.ai.agents.Agent;
import sg.games.pirate.entities.controls.WeaponControl;

/**
 *
 * @author cuongnguyen
 */
public class GameAgent<T> extends Agent<T> {

    /**
     * AbstractWeapon used by agent.
     */
    private WeaponControl weaponControl;

    public GameAgent(String name) {
        super(name);
    }

    public GameAgent(String name, Spatial spatial) {
        super(name, spatial);
    }

    @Override
    protected void controlUpdate(float tpf) {
        super.controlUpdate(tpf);

        if (weaponControl != null) {
            weaponControl.update(tpf);
        }
    }

    /**
     * @return weapon that agent is currently using
     */
    public WeaponControl getWeaponControl() {
        return weaponControl;
    }

    /**
     * It will add weapon to agent and add its spatial to agent, if there
     * already was weapon before with its own spatial, it will remove it before
     * adding new weapon spatial.
     *
     * @param weapon that agent will use
     */
    public void setWeaponControl(WeaponControl weapon) {
        //        //remove previous weapon spatial
        //        if (this.weapon != null && this.weapon.getSpatial() != null) {
        //            this.weapon.getSpatial().removeFromParent();
        //        }
        //        //add new weapon spatial if there is any
        //        if (weapon.getSpatial() != null) {
        //            ((Node) spatial).attachChild(weapon.getSpatial());
        //        }
        this.weaponControl = weapon;
    }

}
