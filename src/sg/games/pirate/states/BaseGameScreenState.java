package sg.games.pirate.states;

import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;
import sg.atom.core.execution.BaseGameState;
import sg.games.pirate.PirateMain;
import tonegod.gui.core.Screen;

/**
 *
 * @author cuongnguyen
 */
public class BaseGameScreenState extends BaseGameState {

    protected Screen screen;
    protected Node screenNode;
    protected float currentTime = 0;
    protected String screenName;
    protected String screenStyle;
    protected String screenAtlas;
    protected String screenAtlasTexture;
    protected boolean useAtlas = false;
    protected float SW = 0;
    protected float SH = 0;

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        AppSettings settings = getApp().getSettings();
        SW = settings.getWidth();
        SH = settings.getHeight();
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (enabled) {
            createUI();
        } else {
            removeUI();
        }
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        currentTime += tpf;
    }

    public void createUI() {
        if (!useAtlas) {
            createScreen();
        } else {
            createScreenAtlas();
        }
        createScreenElements();
        showScreen();
    }

    public void removeUI() {
    }

    public void createScreenElements() {
    }

    public Screen getScreen() {
        return screen;
    }

    public Node getScreenNode() {
        return screenNode;
    }

    public float getCurrentTime() {
        return currentTime;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getScreenStyle() {
        return screenStyle;
    }

    public void setScreenStyle(String screenStyle) {
        this.screenStyle = screenStyle;
    }

    public String getScreenAtlas() {
        return screenAtlas;
    }

    public void setScreenAtlas(String screenAtlas) {
        this.screenAtlas = screenAtlas;
    }

    public void createScreen() {
        screenNode = new Node(getScreenName());
        screen = new Screen(app);
        screenNode.addControl(screen);
    }

    public void createScreenAtlas() {
        screenNode = new Node();
        screen = new Screen(this.app, getScreenAtlas());
        screen.setUseTextureAtlas(true, getScreenAtlasTexture());
        screenNode.addControl(screen);
    }

    public String getScreenAtlasTexture() {
        return screenAtlasTexture;
    }

    public void setScreenAtlasTexture(String screenAtlasTexture) {
        this.screenAtlasTexture = screenAtlasTexture;
    }

    public boolean isUseAtlas() {
        return useAtlas;
    }

    public void setUseAtlas(boolean useAtlas) {
        this.useAtlas = useAtlas;
    }

    public void showScreen() {
        app.getGuiNode().attachChild(screenNode);
    }

    public void hideScreen() {
        app.getGuiNode().detachChild(screenNode);
    }

    @Override
    public void stateDetached(AppStateManager stateManager) {
        screenNode.removeControl(screen);
        screenNode.removeFromParent();
        app.getInputManager().removeRawInputListener(screen);
        super.stateDetached(stateManager);
    }

    @Override
    public PirateMain getApp() {
        return (PirateMain) super.getApp(); //To change body of generated methods, choose Tools | Templates.
    }

}
