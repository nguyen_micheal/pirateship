package sg.games.pirate.states;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.ui.Picture;

/**
 *
 * @author CuongNguyen
 */
public class SplashState extends BaseGameScreenState {

    float activeTime = 0, lastTime = 0, stepTime = 3;
    private Picture logo;

    public SplashState() {
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);

        setEnabled(true);
    }

    public void createUI() {
        this.app.getViewPort().setBackgroundColor(ColorRGBA.LightGray);
        createLogo();
    }

    public void createLogo() {
        logo = new Picture("Logo");
        logo.setImage(assetManager, "Interface/Images/Brand/Logo_SGGame.png", true);
        this.guiNode.attachChild(logo);

    }

    @Override
    public void removeUI() {
        super.removeUI();
        logo.removeFromParent();
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);

        activeTime += tpf;

        if (activeTime - lastTime > stepTime) {
            lastTime = activeTime;
            toState(MainMenuState.class);
        } else {
            float logoScale = 200 * (1 + 0.5f * FastMath.sin(activeTime));
            logo.setLocalScale(logoScale);
            float sw = app.getSettings().getWidth();
            float sh = app.getSettings().getHeight();
            logo.setLocalTranslation(sw / 2 - logoScale / 2, sh / 2 - logoScale / 2, 0);
        }
    }

    public void toState(Class<? extends AbstractAppState> newState) {
        if (newState.isAssignableFrom(MainMenuState.class)) {
            app.getStateManager().detach(this);
            app.getStateManager().attach(new MainMenuState());
        } else {
            throw new IllegalStateException("Can not change to new state!" + newState.getName());
        }
    }
}
