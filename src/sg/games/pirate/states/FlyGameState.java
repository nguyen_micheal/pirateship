package sg.games.pirate.states;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.font.BitmapFont;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.FastMath;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.math.Vector4f;
import com.jme3.post.FilterPostProcessor;
import java.util.logging.Logger;
import sg.atom.corex.entity.EntityFunction;
import sg.atom.corex.entity.SpatialEntity;
import sg.atom.corex.stage.IntervalControl;
import sg.atom.corex.ui.tonegod.controls.ExButtonAdapter;
import sg.atom.corex.ui.tonegod.layouts.QuickLayouts;
import sg.games.pirate.gameplay.ShipGamePlay;
import sg.games.pirate.stage.cam.ShipFollowCamera.CameraMoveListener;
import sg.games.pirate.stage.filter.VignetteFilter;
import tonegod.gui.controls.buttons.Button;
import tonegod.gui.controls.buttons.ButtonAdapter;
import tonegod.gui.controls.extras.Indicator;
import tonegod.gui.controls.text.Label;
import tonegod.gui.controls.windows.Panel;
import tonegod.gui.controls.windows.Window;
import tonegod.gui.core.Element;
import tonegod.gui.core.Element.Orientation;
import tonegod.gui.core.layouts.LayoutHint;
import tonegod.gui.effects.AbstractElementEffect;
import tonegod.gui.effects.Effect;

/**
 *
 * @author cuong.nguyen
 */
public class FlyGameState extends BaseGameScreenState implements CameraMoveListener {

    private static final Logger logger = Logger.getLogger(FlyGameState.class.getName());
    protected boolean gameStarted = false;
    protected boolean gamePause = false;
    protected ActionListener actionListener;
    private QuickLayouts layout;
    private Panel actionPanel;
    private Panel onScreenButtons;
    private Label lblGemNum;
    private Label lblKillNum;
    private Label lblScore;
    private Label lblCountDown;
    private Panel costDialogPanel;
    private Label lblCostText;
    private Window gameResultDialog;

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);

        setEnabled(true);
    }

    @Override
    public void goToState(Class<? extends AbstractAppState> newState) {
        if (newState.isAssignableFrom(MainMenuState.class)) {
            app.getStateManager().detach(this);
            app.getStateManager().attach(new MainMenuState());
        } else {
            throw new IllegalStateException("Can not change to new state!" + newState.getName());
        }
    }

    @Override
    public void createScreenElements() {
//        createScreenEffects();

        layout = new QuickLayouts(screen, app.getGlobalAssetCache());

        Panel topPanel = new Panel(screen, "topPanel", Vector2f.ZERO, new Vector2f(SW, SH * 0.1f));
        topPanel.setAsContainerOnly();

        createAvatarBar();
        createTopButtons(topPanel);
        screen.addElement(topPanel);

        actionPanel = new Panel(screen, "actionPanel", new Vector2f(0, SH - SW * 0.06f), new Vector2f(SW, SH * 0.1f));
        actionPanel.setAsContainerOnly();
        createBuildItemButtons();
        screen.addElement(actionPanel, true);

        onScreenButtons = new Panel(screen, "onScreenButtons", new Vector2f(0, 0), new Vector2f(SW * 0.3f, SW * 0.3f));
        onScreenButtons.setAsContainerOnly();
        screen.addElement(onScreenButtons, true);

//        createGameStatusUI();
    }

    protected void createScreenEffects() {
        FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
        VignetteFilter vignetteFilter = new VignetteFilter();
        fpp.addFilter(vignetteFilter);
        app.getViewPort().addProcessor(fpp);
    }

    private void createGameStatusUI() {
        Indicator waveIndicator = new Indicator(screen, "waveIndicator", new Vector2f(SW * 0.25f, SH * 0.85f), new Vector2f(SW * 0.5f, SH * 0.05f), Orientation.HORIZONTAL) {

            @Override
            public void onChange(float currentValue, float currentPercentage) {

            }
        };
        waveIndicator.setBaseImage("Interface/Images/Panels/Gauge.png");
        waveIndicator.setDisplayPercentage();
        waveIndicator.setDisplayValues();
        waveIndicator.setIndicatorImage("Interface/Images/Panels/Gauge2.png");
        waveIndicator.setMaxValue(100);
        waveIndicator.setCurrentValue(10);
//        waveIndicator.setIndicatorColor(ColorRGBA.LightGray);
        screen.addElement(waveIndicator);
    }

    private void createBuildItemButtons() {
        for (int i = 0; i < 5; i++) {
            final float iconSize = SW * 0.05f;
            Button btnBuildItem = layout.buttonSprite(new ButtonAdapter(screen, "btnDragon" + i, new Vector2f(SW * (0.2f + 0.1f * i), 0), new Vector2f(SW * 0.1f, SW * 0.06f)) {
                @Override
                public void onMouseLeftReleased(MouseButtonEvent evt) {
                    super.onMouseLeftReleased(evt);
                }
            }, "UiSkin1", "btn_wood", "btn_wood_disable", "btn_wood_down");
//            btnDragon1.setText("Dragon" + i);
            Element imgDragon = layout.sprite(new Element(screen, "imgChar" + i, new Vector2f(SW * 0.05f - iconSize / 2, 0), new Vector2f(iconSize, iconSize), Vector4f.ZERO, null), "CharacterThumbnails", "Char_" + i);
            btnBuildItem.addChild(imgDragon);
            imgDragon.setIgnoreMouse(true);
            actionPanel.addChild(btnBuildItem);
        }
    }

    private void createAvatarBar() {
//        Element circle1 = layout.sprite(new Element(screen, "Circle1", new Vector2f(0, 0), new Vector2f(SH * 0.25f, SH * 0.25f), Vector4f.ZERO, null), "CommonElements", "Circle_4");
//        screen.addElement(circle1);
//        Element avatar = new Element(screen, "avatar", new Vector2f(SW * 0f, 0), new Vector2f(SH * 0.2f, SH * 0.2f), Vector4f.ZERO, "Interface/Images/Characters/Portraits/black.png");
//        screen.addElement(avatar);

//        Panel healthBarPanel = new Panel(screen, "healthBarPanel", new Vector2f(0, SH * 0.2f), new Vector2f(SW * 0.15f, SH * 0.1f));
//        healthBarPanel.setAsContainerOnly();
//        Element hpBar = layout.sprite(new Element(screen, "hpBar", Vector2f.ZERO, new Vector2f(SW * 0.15f, SH * 0.04f), new Vector4f(5, 5, 5, 5), null), "UiSkin1", "progressBar");
//        healthBarPanel.addChild(hpBar);
//        Element hpBarInner = layout.sprite(new Element(screen, "hpBarInner", new Vector2f(SW * 0.02f, SH * 0f), new Vector2f(SW * 0.1f, SH * 0.03f), new Vector4f(5, 5, 5, 5), null), "UiSkin1", "box_red");
//        hpBar.addChild(hpBarInner);
//
//        Element manaBar = layout.sprite(new Element(screen, "manaBar", new Vector2f(SW * 0, SH * 0.03f), new Vector2f(SW * 0.15f, SH * 0.04f), new Vector4f(5, 5, 5, 5), null), "UiSkin1", "progressBar");
//        Element manaBarInner = layout.sprite(new Element(screen, "manaBarInner", new Vector2f(SW * 0.02f, SH * 0f), new Vector2f(SW * 0.1f, SH * 0.03f), new Vector4f(5, 5, 5, 5), null), "UiSkin1", "box_green");
//        manaBar.addChild(manaBarInner);
//
//        healthBarPanel.addChild(hpBar);
//        healthBarPanel.addChild(manaBar);
//
//        screen.addElement(healthBarPanel);
    }

    private void createTopButtons(Panel topPanel) {
        Vector2f panelSize = new Vector2f(SW * 0.1f, SW * 0.05f);
        Vector2f subButtonSize = new Vector2f(panelSize.y * 0.6f, panelSize.y * 0.6f);

        Element panelGem = layout.sprite(new Panel(screen, "panelGem", new Vector2f(SW * 0.3f, SH * 0.0f), panelSize), "UiSkin2", "panel_round_golden");
        panelGem.setIsResizable(false);
        panelGem.setIsMovable(false);
        Element iconGem = layout.sprite(new Panel(screen, "iconGem", new Vector2f(-panelSize.x * 0.1f, panelSize.y * 0.1f), new Vector2f(panelSize.y * 0.8f, panelSize.y * 0.8f)), "UiSkin2", "icon_gem");
        iconGem.setIsResizable(false);
        panelGem.addChild(iconGem);
        lblGemNum = new Label(screen, "lblGemNum", new Vector2f(panelSize.x * 0.3f, SH * 0.0f), panelSize);
        lblGemNum.setFont("Interface/Fonts/lifecraft.fnt");
        lblGemNum.setText("100");
        Button btnAddGem = layout.buttonSprite(new ButtonAdapter(screen, "btnAddGem", new Vector2f(panelSize.x * 0.8f, SW * 0.01f), subButtonSize) {
            @Override
            public void onMouseLeftReleased(MouseButtonEvent evt) {
                super.onMouseLeftReleased(evt);
//                bottomPanel.showWithEffect();
            }
        }, "UiSkin1", "btnPlus_up", "btnPlus_down", "btnPlus_down");
        panelGem.addChild(btnAddGem);
        panelGem.addChild(lblGemNum);
        topPanel.addChild(panelGem);

        //
        Element panelKill = layout.sprite(new Panel(screen, "panelKill", new Vector2f(SW * 0.45f, SH * 0.0f), panelSize), "UiSkin2", "panel_round_golden");
        panelKill.setIsResizable(false);
        panelKill.setIsMovable(false);
        lblKillNum = new Label(screen, "lblKillNum", new Vector2f(panelSize.x * 0.3f, SH * 0.0f), panelSize);
        lblKillNum.setFont("Interface/Fonts/lifecraft.fnt");
        lblKillNum.setText("100");

        panelKill.addChild(lblKillNum);
        topPanel.addChild(panelKill);

        //
        Element panelScore = layout.sprite(new Panel(screen, "panelScore", new Vector2f(SW * 0.6f, SH * 0.0f), panelSize), "UiSkin2", "panel_round_golden");
        panelScore.setIsResizable(false);
        panelScore.setIsMovable(false);
        lblScore = new Label(screen, "lblScore", new Vector2f(panelSize.x * 0.3f, SH * 0.0f), panelSize);
        lblScore.setFont("Interface/Fonts/lifecraft.fnt");
        lblScore.setText("100");
        panelScore.addChild(lblScore);
        topPanel.addChild(panelScore);
    }

    public void setupInput() {
        actionListener = new ActionListener() {
            public void onAction(String name, boolean keyPressed, float tpf) {
            }
        };
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        if (initialized) {
            if (gameStarted) {
                if (!gamePause) {
                    updateUI(tpf);
                }
            } else {
                if (getApp().getGamePlayManager().isActived()) {
                    load();
                    System.out.println("InGameState startGame");
                    startGame();
                    createCountDown(null);
                    gameStarted = true;
                }
            }
        } else {
        }
    }

    public void load() {
        app.getEntityManager().load();
        app.getStageManager().load();
        app.getWorldManager().load();
        getApp().getGamePlayManager().load();
    }

    public void pauseGame() {
    }

    public void startGame() {
        setupInput();
        getApp().getStageManager().onStageReady();
        getApp().getGamePlayManager().startGame();
    }

    public void restartGame() {
    }

    public void resumeGame() {
    }

    //GETTER & SETTER
    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (initialized) {
            if (enabled) {
            } else {
            }
        }
    }

    @Override
    public void stateDetached(AppStateManager stateManager) {
        super.stateDetached(stateManager);
    }

    private void createInstructor() {
        //Intructor character
        Element instructorCharacter = new Element(screen, "instructorCharacter", new Vector2f(SW * 0f, SH * 0.6f), new Vector2f(SH * 0.4f, SH * 0.4f), Vector4f.ZERO, "Interface/Images/Characters/Portraits/princess.png");
        screen.addElement(instructorCharacter);
    }

    private void createGamePlayNotification() {

    }

    private void createCountDown(final Runnable onFinish) {
        if (lblCountDown == null) {
            lblCountDown = new Label(screen, new Vector2f(SW * 0.3f, SH * 0.5f), new Vector2f(SW * 0.4f, SH * 0.1f));
            lblCountDown.setFont("Interface/Fonts/digits/yellow_big_digit.fnt");
//            lblCountDown.setText("1");
            lblCountDown.setFontSize(50);
            lblCountDown.setTextAlign(BitmapFont.Align.Center);
//        lblCountDown.setOrigin(SW, SW);
            screen.addElement(lblCountDown);
        } else {
//            lblCountDown.setText("1");
        }
        lblCountDown.setIsVisible(true);
        IntervalControl intervalControl = new IntervalControl(onFinish) {
            int currentNum = 3;
            int targetCount = 0;

            @Override
            public boolean isFinish(float activeTime) {
                return currentNum < targetCount;
            }

            @Override
            public void invoke(float activeTime) {
                lblCountDown.setText(Integer.toString(currentNum));
                currentNum--;
            }

            @Override
            public void onFinish() {
                ((Element) getSpatial()).setIsVisible(false);
                super.onFinish();
            }

        };
        lblCountDown.addControl(intervalControl);
        intervalControl.setEnabled(true);

    }

    private void createGameResultDialog() {
        if (gameResultDialog == null) {
            gameResultDialog = new Window(screen, new Vector2f(SW * 0.25f, SH * 0.25f), new Vector2f(SW * 0.5f, SH * 0.5f));
            screen.addElement(gameResultDialog, true);
            gameResultDialog.addEffect(new Effect(Effect.EffectType.SlideIn, AbstractElementEffect.EffectEvent.Show, 0.5f));
            gameResultDialog.addEffect(new Effect(Effect.EffectType.SlideOut, AbstractElementEffect.EffectEvent.Hide, 0.25f));
        }
        gameResultDialog.showWithEffect();
    }

    private void createGameOverDialog() {

    }

    private void createGamePauseDialog() {

    }

    private void updateUI(float tpf) {
        ShipGamePlay currentGamePlay = getApp().getGamePlayManager().getCurrentGamePlay();
        lblGemNum.setText(Integer.toString(currentGamePlay.getGold()) + " / " + currentGamePlay.getTotalCoins());
    }

    public void showActionPanel(boolean state) {
        actionPanel.setIsVisible(state);
    }

    public void showOnScreenButtons(boolean state, SpatialEntity slot) {
        if (slot != null) {
            createOnScreenButtons(slot);
        }
        if (state) {
            Vector3f spatialScreenPos = getScreenPos(slot);
            onScreenButtons.setPosition(spatialScreenPos.x, spatialScreenPos.y - SH / 2);
        }
        onScreenButtons.setIsVisible(state);
    }

    private void createOnScreenButtons(final SpatialEntity slot) {
        onScreenButtons.removeAllChildren();
        int numOfButtons = slot.getFunctions().size();
        for (int i = 0; i < numOfButtons; i++) {

            //Circle
            float angle = -FastMath.PI / (numOfButtons - 1) * i;
            float radius = SW * 0.1f;
            Vector2f pos = new Vector2f(FastMath.cos(angle) * radius, FastMath.sin(angle) * radius);

            createEntityFunctionButton(i, slot, pos);
        }
    }

    protected void createEntityFunctionButton(int i, final SpatialEntity slot, Vector2f pos) {
        //Entity functions?
        final Integer actionType = i;
        final EntityFunction entityFunction = slot.getFunctions().get(i);
        Button btnBuildItem = new ExButtonAdapter(screen, "btnOnScreenItem" + actionType, pos, new Vector2f(SW * 0.1f, SW * 0.06f), layout, "UiSkin1", "btn_wood", "btn_wood_disable", "btn_wood_down") {
            @Override
            public void onMouseLeftReleased(MouseButtonEvent evt) {
                super.onMouseLeftReleased(evt);

                System.out.println("Click on screen button");
                entityFunction.invoke(slot);
                showOnScreenButtons(false, null);
            }
        };
        layout.origin(btnBuildItem, LayoutHint.Align.center);

        float iconSize = SW * 0.08f;
        Element onScreenItemIcon = layout.sprite(new Element(screen, "onScreenItemIcon_" + entityFunction.getName(), new Vector2f(0, -iconSize * 0.2f), new Vector2f(iconSize, iconSize), Vector4f.ZERO, null), "CommonIcons", entityFunction.getIcon());
        btnBuildItem.addChild(onScreenItemIcon);
        onScreenItemIcon.setIgnoreMouse(true);
        onScreenButtons.addChild(btnBuildItem);
    }

    private Vector3f getScreenPos(SpatialEntity spatialEntity) {
        return getApp().getCamera().getScreenCoordinates(spatialEntity.getSpatial().getWorldTranslation());
    }

    public void showCostDialog(boolean canPay, int amount) {
        if (costDialogPanel == null) {
            costDialogPanel = new Panel(screen, "costDialogPanel", new Vector2f(SW * 0.25f, SH * 0.9f), new Vector2f(SW * 0.5f, SH * 0.1f));
            lblCostText = new Label(screen, "lblCostText", new Vector2f(SW * 0.05f, SH * 0.0f), new Vector2f(SW * 0.5f, SH * 0.1f));
            costDialogPanel.addChild(lblCostText);
            lblCostText.setText("ABC");
            screen.addElement(costDialogPanel);
        } else {
            costDialogPanel.setIsVisible();
        }
        if (canPay) {
            lblCostText.setText("You can pay" + amount);
        } else {
            lblCostText.setText("You can not pay " + amount + " .Please buy more gem!");
        }
    }

    public void onCamMove(int degree, float amount) {
        showOnScreenButtons(false, null);
    }

}
