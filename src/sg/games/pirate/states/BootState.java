package sg.games.pirate.states;

import com.jme3.app.state.AbstractAppState;

/**
 * BootState, the very first state of common mobile games.
 * <li>Load profile, load data, load new content
 * <li>Connect to social network and leader board
 * <li>
 * @author CuongNguyen
 */
public class BootState extends AbstractAppState {
    
}
