package sg.games.pirate;

import com.jme3.system.AppSettings;
import sg.atom.core.AtomMain;
import sg.atom.corex.config.DeviceSettings;
import sg.atom.corex.entity.EntityManager;
import sg.games.pirate.entities.GameEntityFactory;
import sg.games.pirate.gameplay.GamePlayManager;
import sg.games.pirate.gameplay.LevelManager;
import sg.games.pirate.states.FlyGameState;

/**
 * Main class
 */
public class PirateMain extends AtomMain {

    public static DeviceSettings deviceSettings = DeviceSettings.Phone;
    /**
     * Singleton reference of DragonDefenseMain.
     */
    private static PirateMain instance;
    private LevelManager levelManager;
    private GameAssets gameAssets;

    /**
     * Constructs singleton instance of DragonDefenseMain.
     */
    public PirateMain() {
    }

    /**
     * Provides reference to singleton object of DragonDefenseMain.
     *
     * @return Singleton instance of DragonDefenseMain.
     */
    public static synchronized final PirateMain getInstance() {
        if (instance == null) {
            instance = new PirateMain();
        }
        return instance;
    }

    public static void main(String[] args) {
        PirateMain app = PirateMain.getInstance();
        AppSettings cfg = new AppSettings(true);
        deviceSettings.landscape = true;
        cfg.setResolution(deviceSettings.getWidth(), deviceSettings.getHeight());
        cfg.setTitle("Pirate");
        app.setShowSettings(false);
        app.setSettings(cfg);
        app.setDisplayStatView(false);
        app.start();
    }

    @Override
    protected void initAssets() {
        super.initAssets();
        this.gameAssets = new GameAssets();
        this.gameAssets.initialize(stateManager, this);
        this.gameAssets.loadAssets();
    }

    @Override
    public void initManagers() {
        super.initManagers();
        this.entityManager = new EntityManager(this);
        this.entityManager.setEntityFactory(new GameEntityFactory(this));
        entityManager.getEntityFactory().setEntityPackageName("sg.games.pirate.entities");
        this.gamePlayManager = new GamePlayManager(this);
        this.levelManager = new LevelManager(this);
    }

    @Override
    protected void activeManagers() {
        super.activeManagers();

        this.stateManager.attach(stageManager);
        this.stateManager.attach(worldManager);
        this.stateManager.attach(soundManager);
        this.stateManager.attach(guiManager);
        this.stateManager.attach(levelManager);
        this.stateManager.attach(gamePlayManager);
    }

    @Override
    public void initStates() {
//        stateManager.attach(new SplashState());
//        stateManager.attach(new MainMenuState());
//        stateManager.attach(new LoginStateSocket());
        stateManager.attach(new FlyGameState());
    }

    public LevelManager getLevelManager() {
        return levelManager;
    }

    public GamePlayManager getGamePlayManager() {
        return (GamePlayManager) super.getGamePlayManager();
    }

    @Override
    public boolean isDebugMode() {
        return true;
    }

}
