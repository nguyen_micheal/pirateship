package sg.games.pirate.gameplay;

import java.util.List;
import sg.atom.core.datastructure.collection.Array;

/**
 *
 * @author cuong.nguyen
 */
public class GameLevel {

    int index;
    int order;
    String name;
    String description;
    String path;
    String icon;
    String image;
    int requiredStars;
    int stars;
    int status;
    boolean locked;
    Array<Wave> waves;

    public GameLevel() {
        waves = new Array<Wave>();
    }

    public GameLevel(int index, int order, String name, String description, String path, String icon, String image, int status, boolean locked) {
        this.index = index;
        this.order = order;
        this.name = name;
        this.description = description;
        this.path = path;
        this.icon = icon;
        this.status = status;
        this.locked = locked;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getRequiredStars() {
        return requiredStars;
    }

    public void setRequiredStars(int requiredStars) {
        this.requiredStars = requiredStars;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public Array<Wave> getWaves() {
        return waves;
    }

    public void setWaves(Array<Wave> waves) {
        this.waves = waves;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
