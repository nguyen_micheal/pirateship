package sg.games.pirate.gameplay;

import java.util.ArrayList;
import java.util.HashMap;
import sg.atom.core.io.json.Json;
import sg.atom.core.io.json.JsonValue;
import sg.atom.corex.entity.SpatialEntity;

/**
 * Basic mechanism of Tower defense game.
 *
 * @author cuong.nguyen
 */
public class Wave {

    int waveIndex = 0;
    float waveTime = 1;
    int spawnNum = 10;
    String spawnType;
    int pathIndex = 0;
    float interval = 1;
    HashMap<String, Integer> numUnits;
    HashMap<String, Integer> results;
    ArrayList<SpatialEntity> spawnedEntities;

    public Wave() {
    }

    public static interface WaveListener {
    }

    public void init() {
    }

    public void start() {
    }

    public void end() {
    }

    public void update(float tpf) {
    }

    public HashMap<String, Integer> getNumUnits() {
        return numUnits;
    }

    public HashMap<String, Integer> getResults() {
        return results;
    }

//    @Override
//    public void write(Json json) {
//        // TODO Auto-generated method stub
//    }
//
//    @Override
//    public void read(Json json, JsonValue jsonData) {
////        numUnits = json.readValue(HashMap.class, Integer.class, jsonData);
//
//    }

    public ArrayList<SpatialEntity> getSpawnedEntities() {
        return spawnedEntities;
    }

    public void setSpawnType(String spawnType) {
        this.spawnType = spawnType;
    }

    public String getSpawnType() {
        return spawnType;
    }

    public int getPathIndex() {
        return pathIndex;
    }

    public void setPathIndex(int pathIndex) {
        this.pathIndex = pathIndex;
    }

    public int getSpawnNum() {
        return spawnNum;
    }

    public void setSpawnNum(int spawnNum) {
        this.spawnNum = spawnNum;
    }

    public float getInterval() {
        return interval;
    }

    public void setInterval(float interval) {
        this.interval = interval;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Wave:");
        sb.append("\n");
        sb.append("Interval: ");
        sb.append(interval);
        sb.append("\n");
        sb.append("spawnType: ");
        sb.append(spawnType);
        return sb.toString();
    }

}
