package sg.games.pirate.gameplay;

/**
 *
 * @author cuongnguyen
 */
public class Cost implements Comparable<Cost> {

    public int gold = 10;
    public int gem = 10;
    public int mana = 10;

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public int getGem() {
        return gem;
    }

    public void setGem(int gem) {
        this.gem = gem;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.gold;
        hash = 59 * hash + this.gem;
        hash = 59 * hash + this.mana;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cost other = (Cost) obj;
        if (this.gold != other.gold) {
            return false;
        }
        if (this.gem != other.gem) {
            return false;
        }
        if (this.mana != other.mana) {
            return false;
        }
        return true;
    }

    public int compareTo(Cost other) {
        return gold - other.gold;
    }

}
