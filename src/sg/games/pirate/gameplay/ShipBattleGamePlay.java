package sg.games.pirate.gameplay;

import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import com.jme3.audio.AudioNode;
import com.jme3.audio.LowPassFilter;
import com.jme3.collision.CollisionResults;
import com.jme3.effect.ParticleEmitter;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Ray;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.post.FilterPostProcessor;
import com.jme3.renderer.Camera;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.texture.Texture2D;
import com.jme3.util.SkyFactory;
import com.jme3.water.WaterFilter;
import java.util.ArrayList;
import sg.atom.ai.agents.AgentTeam;
import sg.atom.ai.agents.control.AgentsAppState;
import sg.atom.corex.entity.SpatialEntity;
import sg.atom.corex.managers.WorldManager;
import sg.atom.corex.world.road.Path;
import sg.atom.corex.world.select.SelectManager;
import sg.atom.corex.world.select.SelectManager.SelectListener;
import sg.games.pirate.entities.BaseShooterCharacter;
import sg.games.pirate.entities.GameEntityFactory;
import sg.games.pirate.entities.PlayerShip;
import sg.games.pirate.stage.cam.ShipFollowCamera;
import sg.games.pirate.states.FlyGameState;

/**
 * DefenseGamePlay
 *
 * Wave
 *
 * Agent
 *
 * Score
 *
 * @author cuongnguyen
 */
public class ShipBattleGamePlay extends BaseGamePlay implements SelectListener {

    int ammo = 100, maxAmmo = 100;
    int gunType;
    int gunStatus;
    float gunRange;
    ShipFollowCamera camState;

    GameLevel currentLevel;
    int waveCount = 0;
    int waveIndex = 0;
    int life = 0;
    int scoreKilled = 0;
    float timePassed = 0;
    float totalTime = 0;
    int gold = 0;

    Wave currentWave;
    SelectManager selectManager;
    AgentsAppState agentsAppState;
    AgentTeam team1;
    AgentTeam team2;

    ArrayList<Path> paths;
    WaveSpawner waveSpawner;
    PlayerShip playerShip;
    int totalCoins;

    public Vector3f lightDir = new Vector3f(-4.9236743f, -1.27054665f, 5.896916f);
    public WaterFilter water;
    AudioNode waveSounds;
    LowPassFilter underWaterAudioFilter = new LowPassFilter(0.5f, 0.1f);
    LowPassFilter underWaterReverbFilter = new LowPassFilter(0.5f, 0.1f);
    LowPassFilter aboveWaterAudioFilter = new LowPassFilter(1, 1);
    public float time = 0.0f;
    public float waterHeight = 0.0f;
    public float initialWaterHeight = 1f;
    public boolean isUnderWater = false;

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        setEnabled(false);

        this.camState = new ShipFollowCamera();
        stateManager.attach(camState);
        camState.setEnabled(false);
    }

    public void resetAttributes() {
        waveCount = 0;
        waveIndex = 0;
        life = 100;
        scoreKilled = 0;
        timePassed = 0;
        totalTime = 0;
        //FIXME: Get from Player profile
        gold = 1000;
    }

    public void load() {
    }

    public void start() {
        setEnabled(true);
        startLevel(getApp().getLevelManager().getCurrentLevel());
//        createAgents();
//        startWave();
    }

    public void createAgents() {
        agentsAppState = AgentsAppState.getInstance();
        app.getStateManager().attach(agentsAppState);
        team1 = new AgentTeam("Team1");
        team2 = new AgentTeam("Team2");
        agentsAppState.start();
    }

    public void startLevel(GameLevel level) {
        this.currentLevel = level;
        createLevelNode();
        createEntities();
//        processMap(level);
        resetAttributes();

        camState.addListener(getInGameState());
        camState.setEnabled(true);
        camState.setPlayerShip(playerShip);
    }

    private void createLevelNode() {
        Node levelNode = (Node) getApp().getAssetManager().loadModel("Scenes/Sea.j3o");
        getApp().getWorldManager().getWorldNode().attachChild(levelNode);

        createGroupNodes();
//        createLights(levelNode);
        createSky(levelNode);
        setupCamera();
        createOcean();
    }

    private void createGroupNodes() {
        WorldManager worldManager = getApp().getWorldManager();
        worldManager.createGroupNode("Ship");
        worldManager.createGroupNode("Bullet");
    }

    private void createOcean() {
        //Water Filter
        water = new WaterFilter(rootNode, lightDir);
        water.setWaterColor(new ColorRGBA().setAsSrgb(0.0078f, 0.5f, 0.6f, 1.0f));
        water.setDeepWaterColor(new ColorRGBA().setAsSrgb(0.0039f, 0.00196f, 0.145f, 1.0f));
        water.setUnderWaterFogDistance(80);
        water.setWaterTransparency(0.62f);
        water.setFoamIntensity(0.1f);
        water.setFoamHardness(0.3f);
        water.setFoamExistence(new Vector3f(0.8f, 8f, 1f));
        water.setReflectionDisplace(50);
        water.setRefractionConstant(0.25f);
        water.setColorExtinction(new Vector3f(30, 50, 70));
        water.setCausticsIntensity(0.0f);
        water.setWaveScale(0.003f);
        water.setMaxAmplitude(2f);
        water.setFoamTexture((Texture2D) assetManager.loadTexture("Common/MatDefs/Water/Textures/foam2.jpg"));
        water.setRefractionStrength(0.1f);
        water.setWaterHeight(initialWaterHeight);

        FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
        fpp.addFilter(water);
        getApp().getViewPort().addProcessor(fpp);

        isUnderWater = getApp().getCamera().getLocation().y < waterHeight;
//        setupFoamControl();
    }

    private void createSky(Node levelNode) {
        Spatial sky = SkyFactory.createSky(assetManager, "Scenes/Beach/FullskiesSunset0068.dds", false);
        sky.setLocalScale(350);
        levelNode.attachChild(sky);
    }

    private void createLights(Node levelNode) {
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(lightDir);
        sun.setColor(ColorRGBA.White.clone().multLocal(1f));
        levelNode.addLight(sun);

        AmbientLight al = new AmbientLight();
        al.setColor(new ColorRGBA(0.1f, 0.1f, 0.1f, 1.0f));
        levelNode.addLight(al);
    }

    private void setupCamera() {
    }

    private void createSounds() {
        waveSounds = new AudioNode(assetManager, "Sound/Environment/Ocean Waves.ogg", false);
        waveSounds.setLooping(true);
        waveSounds.setReverbEnabled(true);
        if (isUnderWater) {
            waveSounds.setDryFilter(new LowPassFilter(0.5f, 0.1f));
        } else {
            waveSounds.setDryFilter(aboveWaterAudioFilter);
        }
        getApp().getAudioRenderer().playSource(waveSounds);
    }

    protected void processMap(GameLevel level) {
//        waveSpawner = new WaveSpawner(level, this, 1) {
//            @Override
//            public void spawn(float totalTime) {
////                System.out.println("Spawn " + getCurrentWave().getSpawnType());
//                Path path = paths.get(getCurrentWave().getPathIndex());
//                BaseShooterCharacter soldier = (BaseShooterCharacter) createEntity(getCurrentWave().getSpawnType(), path.getStart());
//                soldier.getAgent().setTeam(team2);
//                soldier.getAgent().getBehaviour(PathFollow.class).setPath(path);
//            }
//
//        };
    }

    public void endLevel(GameLevel level) {
    }

    public void createEntities(GameLevel level) {
    }

    public SpatialEntity createEntity(String type, Vector2f pos) {
        GameEntityFactory factory = (GameEntityFactory) app.getEntityManager().getEntityFactory();
        SpatialEntity newEntity = (SpatialEntity) factory.create(type);
        app.getWorldManager().putEntity(app.getWorldManager().getGroupNode(type), newEntity, pos);
        return newEntity;
    }

    public SpatialEntity createEntity(String type, Vector3f pos) {
        GameEntityFactory factory = (GameEntityFactory) app.getEntityManager().getEntityFactory();
        SpatialEntity newEntity = (SpatialEntity) factory.create(type);
        app.getWorldManager().putEntity(app.getWorldManager().getGroupNode(type), newEntity, pos);
        return newEntity;
    }

    protected void createEntities() {
        GameEntityFactory factory = (GameEntityFactory) app.getEntityManager().getEntityFactory();
        playerShip = factory.createPlayerShip();
        app.getWorldManager().putEntity(playerShip, Vector3f.ZERO);
        createItems();
    }

    protected void createItems() {
        GameEntityFactory factory = (GameEntityFactory) app.getEntityManager().getEntityFactory();
        totalCoins = 0;
    }

    void startWave() {
//        currentWave = currentLevel.getWaves().get(waveIndex);
//        waveSpawner.setStarted(true);
//        waveSpawner.setCurrentWave(currentWave);

    }

    void updateWave(float tpf) {
    }

    void nextWave() {
    }

    void endWave() {
    }

    boolean checkContinue() {
        return true;
    }

    @Override
    public void update(float tpf) {
//        System.out.println("Update TowerDefense Gameplay");
        if (isEnabled()) {
            super.update(tpf);
            updateScene(tpf);
            updateWave(tpf);
        }
    }

    private void updateScene(float tpf) {
        time += tpf;
        waterHeight = (float) Math.cos(((time * 0.6f) % FastMath.TWO_PI)) * 1.5f;
        water.setWaterHeight(initialWaterHeight + waterHeight);
//        updateSounds(tpf);
    }

    private void updateSounds(float tpf) {
        if (water.isUnderWater() && !isUnderWater) {
            waveSounds.setDryFilter(new LowPassFilter(0.5f, 0.1f));
            isUnderWater = true;
        }
        if (!water.isUnderWater() && isUnderWater) {
            isUnderWater = false;
            //waves.setReverbEnabled(false);
            waveSounds.setDryFilter(new LowPassFilter(1, 1f));
            //waves.setDryFilter(new LowPassFilter(1,1f));
        }
    }

    public void setupInput() {
        //Mouse
        //Key
        inputManager = getApp().getInputManager();
        inputManager.addMapping("Shoot",
                new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        inputManager.addMapping("Reload",
                new KeyTrigger(KeyInput.KEY_SPACE),
                new MouseButtonTrigger(MouseInput.BUTTON_RIGHT));
        inputManager.addListener(actionListener, "Shoot", "Reload");
//        inputManager.addListener(analogListener, "Shoot");
    }
    //Collision checking

    private void shoot() {
        ammo--;
//        logger.info("Shoot!");
        CollisionResults results = new CollisionResults();
        Camera cam = getApp().getCamera();
        Node boxes = getApp().getWorldManager().getCollisionNode();
        Ray ray = new Ray(cam.getLocation(), cam.getDirection());
        boxes.collideWith(ray, results);
//        for (int i = 0; i < results.size(); i++) {
//            float dist = results.getCollision(i).getDistance();
//            Vector3f pt = results.getCollision(i).getContactPoint();
//            logger.info("Collision: " + results.getCollision(i).getGeometry().getName());
//        }

        if (results.size() > 0) {
            Geometry target = results.getClosestCollision().getGeometry();
            Vector3f contactPoint = results.getClosestCollision().getContactPoint();
            SpatialEntity entity = getApp().getEntityManager().getEntityFrom(target);
            if (entity != null) {
                hit(entity);
                hit(target, contactPoint);
            } else {
            }

        }
    }

    void hit(Geometry target, Vector3f contactPoint) {
        if (target != null) {

            ParticleEmitter explosion = getApp().getEffectManager().createExplosion();
            getApp().getWorldManager().putSpatial(explosion, contactPoint);
            explosion.emitAllParticles();
//            score++;
//            logger.info("Hit target!");
            //            target.removeFromParent();
        }
    }

    void hit(SpatialEntity entity) {
//        logger.info("Hit entity" + entity.getIid());
        getApp().getEntityManager().removeEntity(entity);
    }

//    void hit()
    void shootCheck() {
    }

    void reload() {
        ammo = maxAmmo;
    }
    private ActionListener actionListener = new ActionListener() {
        public void onAction(String name, boolean pressed, float tpf) {
            if (name.equals("Shoot") && !pressed) {
                shoot();
//                logger.info("Shoot action");
            } else if (name.equals("Reload") && !pressed) {
                reload();
            }
        }
    };
    private AnalogListener analogListener = new AnalogListener() {
        public void onAnalog(String name, float intensity, float tpf) {
            if (name.equals("Shoot")) {
            }
        }
    };

    public AgentsAppState getAgentsAppState() {
        return agentsAppState;
    }

    public FlyGameState getInGameState() {
        return app.getStateManager().getState(FlyGameState.class);
    }

    public void onSelectSpatialEntity(SpatialEntity spatialEntity) {
    }

    public void onDeselectAll() {
//        getInGameState().showActionPanel(false);
//        getInGameState().showOnScreenButtons(false, null);
    }

    public boolean letPlayerPay(int amount) {
        if (gold - amount >= 0) {
            gold -= amount;
            return true;
        } else {
            getInGameState().showCostDialog(false, amount);
            return false;
        }
    }

    public int getGold() {
        return gold;
    }

    public int getTotalCoins() {
        return totalCoins;
    }

    public void setTotalCoins(int totalCoins) {
        this.totalCoins = totalCoins;
    }

    public boolean isPlayerCanPay(int cost) {
        return gold >= cost;
    }

    @Override
    public boolean isActived() {
        return initialized && camState.isInitialized();
    }

    public int getAmmo() {
        return ammo;
    }
}
