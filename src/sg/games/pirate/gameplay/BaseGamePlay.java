package sg.games.pirate.gameplay;

import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import sg.atom.core.execution.BaseGameState;
import sg.games.pirate.PirateMain;
import sg.games.pirate.entities.GameEntityFactory;

/**
 *
 * @author cuongnguyen
 */
public class BaseGamePlay extends BaseGameState {

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
    }

    public PirateMain getApp() {
        return (PirateMain) app;
    }

    public GameEntityFactory getGameEntityFactory() {
        return (GameEntityFactory) getApp().getEntityManager().getEntityFactory();
    }

    public GamePlayManager getGamePlayManager() {
        return getApp().getGamePlayManager();
    }
}
