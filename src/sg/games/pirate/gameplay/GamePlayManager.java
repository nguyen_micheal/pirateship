package sg.games.pirate.gameplay;

import sg.atom.core.lifecycle.AbstractManager;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sg.atom.core.AtomMain;

/**
 * GamePlayManager represent "GamePlay" and "Player" because we only have one
 * Mode!
 *
 * FIXME:
 *
 * @author cuong.nguyen
 */
public class GamePlayManager extends AbstractManager {

    public static final Logger logger = LoggerFactory.getLogger("GamePlayManager");
    //Gameplay
    int mode;
    int health = 100, maxHP = 100;
    int life = 5;
    int score = 0;
    int scoreKilled = 0;
    float timePassed = 0, totalTime = 100;

    ArrayList<GameLevel> levels;
    GameLevel currentLevel;
    ShipGamePlay currentGamePlay;

    public GamePlayManager(AtomMain app) {
        super(app);
    }

    public void init() {
        super.init();

        currentGamePlay = new ShipGamePlay();
        getApp().getStateManager().attach(currentGamePlay);
        getApp().getWorldManager().createGroupNode("Tank");
    }

    public void load() {
    }

    public void startGame() {
        System.out.println("GamePlayManager startGame");
        currentGamePlay.start();
    }

    void loadLevels() {

    }

    void loadLevel(GameLevel level) {
    }

    void createEntities(GameLevel level) {
    }

    void updateVehicles() {
    }

    void updateGun() {
    }

    public int getHealth() {
        return health;
    }

    public int getScore() {
        return score;
    }

    public ShipGamePlay getCurrentGamePlay() {
        return currentGamePlay;
    }

    public boolean isActived() {
        return initialized && (currentGamePlay != null && currentGamePlay.isActived());
    }

}
