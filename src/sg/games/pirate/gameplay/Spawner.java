package sg.games.pirate.gameplay;

import sg.atom.core.datastructure.collection.Array;
import sg.atom.corex.entity.SpatialEntity;

/**
 *
 * @author cuongnguyen
 */
public abstract class Spawner {

    float totalTime = 0;
    float passedTime = 0;
    float interval = 0;
    boolean started = false;
    GameLevel level;
    int count;
    int maxCount;
    protected Array<SpatialEntity> spawnedEntities;

    public Spawner() {
        this.interval = 3;
        this.count = -1;
    }

    public Spawner(float interval) {
        this.interval = interval;
        this.count = -1;
    }

    public Spawner(float interval, int count) {
        this.interval = interval;
        this.maxCount = count;
    }

    public void start() {
        totalTime = 0;
        started = true;
    }

    public void update(float delta) {
        if (started) {
            passedTime += delta;
            totalTime += delta;
            if (passedTime > interval) {
                passedTime = 0;
                if (count != -1) {
                    count++;
                }
                if (count > maxCount) {
                    started = false;
                    end(totalTime);
                } else {
                    spawn(totalTime);
                }
            } else {
            }
        }
    }

    public boolean canSpawn() {
        if (count > maxCount) {
            return false;
        } else {
            return true;
        }
    }

    public float getTotalTime() {
        return totalTime;
    }

    public float getInterval() {
        return interval;
    }

    public float getPassedTime() {
        return passedTime;
    }

    public void setInterval(float interval) {
        this.interval = interval;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public boolean isStarted() {
        return started;
    }

    public GameLevel getLevel() {
        return level;
    }

    public void setLevel(GameLevel level) {
        this.level = level;
    }

    public abstract void spawn(float totalTime);

    public abstract void end(float totalTime);

    public Array<SpatialEntity> getSpawnedEntities() {
        return spawnedEntities;
    }

    public void clean() {
        if (spawnedEntities != null) {
            for (SpatialEntity u : spawnedEntities) {
                u.removeFromStage();
            }
            spawnedEntities.clear();
        }
    }
}
