package sg.games.pirate.gameplay;

/**
 *
 * @author cuongnguyen
 */
public class WaveSpawner extends Spawner {

    private Wave currentWave;
    private GameLevel gameLevel;
    private ShipBattleGamePlay gamePlay;

    public WaveSpawner(GameLevel gameLevel, ShipBattleGamePlay gamePlay, float interval) {
        super(interval);
        this.gameLevel = gameLevel;
        this.gamePlay = gamePlay;
    }

    public void setLevel(GameLevel gameLevel) {
        this.gameLevel = gameLevel;
    }

    public void setGamePlay(ShipGamePlay gamePlay) {
        this.gamePlay = gamePlay;
    }

    @Override
    public void spawn(float totalTime) {
    }

    @Override
    public void end(float totalTime) {
    }

    public void setCurrentWave(Wave currentWave) {
        this.currentWave = currentWave;
        this.interval = currentWave.getInterval();
        this.count = 0;
        this.maxCount = currentWave.spawnNum;
    }

    public Wave getCurrentWave() {
        return currentWave;
    }

}
