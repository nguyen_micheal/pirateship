package sg.games.pirate.gameplay;

import com.jme3.asset.AssetKey;
import sg.atom.core.AtomMain;
import sg.atom.core.datastructure.collection.Array;
import sg.atom.core.io.json.Json;
import sg.atom.core.lifecycle.AbstractManager;

/**
 *
 * @author cuong.nguyen
 */
public class LevelManager extends AbstractManager {

    Array<GameLevel> levels;
    int currentLevelIndex = 0;

    public LevelManager() {
    }

    public LevelManager(AtomMain app) {
        super(app);
    }

    @Override
    public void init() {
        super.init();
        this.levels = new Array<GameLevel>();
        Json json = new Json();
        String jsonStr = assetManager.loadAsset(new AssetKey<String>("Data/Levels/Levels.json"));
        LevelManager levelData = json.fromJson(LevelManager.class, jsonStr);

        for (GameLevel level : levelData.getLevels()) {
            levels.add(level);
        }
    }

    public Array<GameLevel> getLevels() {
        return levels;
    }

    public GameLevel getLevel(int index) {
        return levels.get(index);
    }

    public GameLevel getCurrentLevel() {
        return levels.get(currentLevelIndex);
    }
}
