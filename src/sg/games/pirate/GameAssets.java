package sg.games.pirate;

import com.jme3.app.Application;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetKey;
import sg.atom.core.execution.BaseGameState;
import sg.atom.corex.assets.GlobalAssetCache;
import sg.atom.corex.assets.sprite.SpriteSheet;

/**
 *
 * @author cuongnguyen
 */
public class GameAssets extends BaseGameState {

    private GlobalAssetCache assetCache;

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.assetCache = stateManager.getState(GlobalAssetCache.class);
        this.initialized = true;
    }

    public void loadAssets() {
        createModelMap();
        createSpriteSheetMap();
    }

    protected void createModelMap() {
        assetCache.modelMap.put("Ship1", "Models/Ship/Ship1/Ship1.j3o");
    }

    protected void createSpriteSheetMap() {
        assetCache.spriteSheets.put("CommonIcons", assetManager.loadAsset(new AssetKey<SpriteSheet>("Interface/Images/CommonIcons.sprites")));
        assetCache.spriteSheets.put("CommonElements", assetManager.loadAsset(new AssetKey<SpriteSheet>("Interface/Images/CommonElements.sprites")));
        assetCache.spriteSheets.put("CharacterThumbnails", assetManager.loadAsset(new AssetKey<SpriteSheet>("Interface/Images/Characters/Thumbnails/Characters.sprites")));
        assetCache.spriteSheets.put("ItemThumbnails", assetManager.loadAsset(new AssetKey<SpriteSheet>("Interface/Images/Items/Items.sprites")));
        assetCache.spriteSheets.put("UiSkin1", assetManager.loadAsset(new AssetKey<SpriteSheet>("Interface/Skin/ui.sprites")));
        assetCache.spriteSheets.put("UiSkin2", assetManager.loadAsset(new AssetKey<SpriteSheet>("Interface/Skin/ui2.sprites")));
    }

}
